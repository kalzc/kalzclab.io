---
layout: post
title: "VmWare NSX LAB in a Box"
date: "2017-06-24"
comments: false
categories: Tools
svg: true
---

VMware NSX is the network virtualization and security platform for the software-defined data center. NSX enables the creation of entire networks in software and embeds them in the hypervisor layer, abstracted from the underlying physical hardware. All network components can be provisioned in minutes, without the need to modify the application.

NSX is a exiting technology that offers flexibility with networks as never before.  NSX offers a nice RESTFUL api which can be integrated with workflow to automate the creation of Isolated Networks

In this post, I present the _NSX Lab in Box_ Architecture for Private Lab for learning

<center>
<embed id="svgimg" type="image/svg+xml" style="display: inline; width: 70%; height: 50%;" src="/images/nsx.svg" />
</center>

NSX provides a full suite of Network and Security solution which helps adress the challenges of MicroSegmentation, IT Automation, IT Optimization and Disaster Recovery.

**Logical Switches**
A cloud deployment or a virtual data center has a variety of applications across multiple tenants. These applications and tenants require isolation from each other for security, fault isolation, and avoiding overlapping IP addressing issues. The NSX logical switch creates logical broadcast domains or segments to which an application or tenant virtual machine can be logically wired. This allows for flexibility and speed of deployment while still providing all the characteristics of a physical network's broadcast domains (VLANs) without physical Layer 2 sprawl or spanning tree issues.
A logical switch is distributed and can span arbitrarily large compute clusters. This allows for virtual machine mobility (vMotion) within the datacenter without limitations of the physical Layer 2 (VLAN) boundary. The physical infrastructure does not have to deal with MAC/FIB table limits since the logical switch contains the broadcast domain in software.

**Logical Routers**
Dynamic routing provides the necessary forwarding information between layer 2 broadcast domains, thereby allowing you to decrease layer 2 broadcast domains and improve network efficiency and scale. NSX extends this intelligence to where the workloads reside for doing East-West routing. This allows more direct virtual machine to virtual machine communication without the costly or timely need to extend hops. At the same time, NSX also provides North-South connectivity, thereby enabling tenants to access public networks.

**Logical Firewall**
Logical Firewall provides security mechanisms for dynamic virtual data centers. The Distributed Firewall component of Logical Firewall allows you to segment virtual datacenter entities like virtual machines based on VM names and attributes, user identity, vCenter objects like datacenters, and hosts as well as traditional networking attributes like IP addresses, VLANs, etc. The Edge Firewall component helps you achieve key perimeter security needs such as building DMZs based on IP/VLAN constructs, tenant to tenant isolation in multi-tenant virtual data centers, Network Address Translation (NAT), partner (extranet) VPNs, and User based SSL VPNs.
The Flow Monitoring feature displays network activity between virtual machines at the application protocol level. You can use this information to audit network traffic, define and refine firewall policies, and identify threats to your network.

**Logical Virtual Private Networks (VPN)s**
SSL VPN-Plus allows remote users to access private corporate applications. IPSec VPN offers site-to-site connectivity between an NSX Edge instance and remote sites. L2 VPN allows you to extend your datacenter by allowing virtual machines to retain network connectivity across geographical boundaries.

**Logical Load Balancer**
The NSX Edge load balancer enables network traffic to follow multiple paths to a specific destination. It distributes incoming service requests evenly among multiple servers in such a way that the load distribution is transparent to users. Load balancing thus helps in achieving optimal resource utilization, maximizing throughput, minimizing response time, and avoiding overload. NSX Edge provides load balancing up to Layer 7.

**Service Composer**
Service Composer helps you provision and assign network and security services to applications in a virtual infrastructure. You map these services to a security group, and the services are applied to the virtual machines in the security group.
Data Security provides visibility into sensitive data stored within your organization's virtualized and cloud environments. Based on the violations reported by NSX Data Security, you can ensure that sensitive data is adequately protected and assess compliance with regulations around the world.

### NSX Components

![alt text](http://i.imgur.com/04Ykujr.png "NSX Components")

NSX Solution is comprised of multiple components as follows

**NSX Manager** - The NSX Manager is the centralized network management component of NSX, and is installed as a virtual appliance on any ESX™ host in your vCenter Server environment. It provides an aggregated system view.

**NSX vSwitch** - NSX vSwitch is the software that operates in server hypervisors to form a software abstraction layer between servers and the physical network.

**NSX Controller** - NSX controller is an advanced distributed state management system that controls virtual networks and overlay transport tunnels.

**NSX Edge** - NSX Edge provides network edge security and gateway services to isolate a virtualized network.

**Distributed Firewall** - Distributed Firewall is a hypervisor kernel-embedded firewall that provides visibility and control for virtualized workloads and networks.

I would encourage to try out NSX capabilities and the level of network control it brings to see the benefits of this solution first hand
