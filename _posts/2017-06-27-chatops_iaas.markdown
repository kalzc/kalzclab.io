---
layout: post
title: "HomeLab with ChatOps, Microservices and IaaS "
date: "2017-06-27"
comments: false
categories: Tutorial
svg: true
---

Did you ever wanted a Private hosted Lab that you can access anywhere? And did you ever wish you can spinup your lab instances on-demand? Ever wished you had a assistant at home always available to poweron/poweroff that Virtualization Host?

Well, you have come to right place, I will guide you in a series of blog posts on how to acheive this using a _Slack Bot_, _Chef_, _Docker_ and a _VMWare Host_

Imagine a sample scenario, you are in an airport and your flight is delayed. And you are itching to try out a new PoC or Develop/Test a new automation script that you were working on. You do not want to stress out your laptop by running local VM's and quickly running out of your battery. So what do we do? 

HOME LAB!!, yes you can spinup your favourite customized lab environment from a _Slack_ and access it over a _WebBrowser_.

So, here your will say something like `lab up` into a slack channel or DM to a **BOT** which will run automation scripts to issue a **WAKE ON LAN** command to your sleeping ESXi/HYPER-V Host.

Following is the component setup I have for this Lab.

<center>
<embed id="svgimg" type="image/svg+xml" style="display: inline; width: 90%; height: 50%;" src="/images/homelab.svg" />
</center>

I have a always-on **Raspberry Pi**, which is running a Docker Container that is serving as a `Slack Bot` and a `Chef Client`.

On the other side of the house, I have plugged in a **UDOO x86** hacker board running few **Docker Containers** to serve my WebFront and Other **Microservices** that I use.

I also have a INTEL Next Unit Computing kit **NUC6I5SYH** Serving as my **ESXi 6.5 host**. 

I dont incurr much running costs on this setup as I power **down** my Docker and ESX Hosts when I dont need them, and power them **ON** via _Slack_ as I show you in this post.

Let's see what is my Lab Status from _Slack_

![alt text](http://i.imgur.com/iVXLvBq.gif "Lab Status")

OK! My Bot is stating that my `ESX HOST is Down`

Let me poweron my **ESX HOST** 

![alt text](http://i.imgur.com/BZE8KLT.gif "PowerOn")

OK! Now check my `lab status`

![alt text](http://i.imgur.com/MKJMRXr.gif "labstatus")

Hmm, this time the bot listed all my VM's hosted on my ESX Host

OK, lets `poweron vm WinX`

![alt text](http://i.imgur.com/avPZTVQ.gif "labstatus")


So, I guess you got the glimpse on what this **ChatOps** look like and hope you are as excited as I am. 

Stay Tuned for my next serving :)

